(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/app.component.html":
/*!**************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/app.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-my-nav></app-my-nav>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/my-nav/my-nav.component.html":
/*!************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/my-nav/my-nav.component.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-sidenav-container class=\"sidenav-container\">\n  <mat-sidenav #drawer class=\"sidenav\" fixedInViewport\n      [attr.role]=\"(isHandset$ | async) ? 'dialog' : 'navigation'\"\n      [mode]=\"(isHandset$ | async) ? 'over' : 'side'\"\n      [opened]=\"(isHandset$ | async) === false\">\n    <mat-toolbar>Menu</mat-toolbar>\n    <mat-nav-list>\n        <a mat-list-item [routerLink]=\"['/detail/1']\" routerLinkActive=\"router-link-active\" >About</a>\n        <a mat-list-item [routerLink]=\"['/add']\" routerLinkActive=\"router-link-active\" >Add</a>\n        <a mat-list-item [routerLink]=\"['/view']\" routerLinkActive=\"router-link-active\" >View</a>\n    </mat-nav-list>\n  </mat-sidenav>\n  <mat-sidenav-content>\n    <mat-toolbar color=\"primary\">\n      <button\n        type=\"button\"\n        aria-label=\"Toggle sidenav\"\n        mat-icon-button\n        (click)=\"drawer.toggle()\"\n        *ngIf=\"isHandset$ | async\">\n        <mat-icon aria-label=\"Side nav toggle icon\">menu</mat-icon>\n      </button>\n      <span>Curriculum Vitae</span>\n    </mat-toolbar>\n    <!-- Add Content Here -->\n    \n    <router-outlet></router-outlet>\n    \n  </mat-sidenav-content>\n</mat-sidenav-container>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/shared/file-not-found/file-not-found.component.html":
/*!***********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/shared/file-not-found/file-not-found.component.html ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h2>The resource you have asked is not in the server</h2>\n<img src=\"assets/images/idiot.jpg\">\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/students/add/students.add.component.html":
/*!************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/students/add/students.add.component.html ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--\n  Old data\n<div class=\"col-md-offset-1 col-md-4\">\n  <form class=\"form-horizontal \">\n    <div class=\"form-group\">\n      <label class=\"col-sm-2 control-label\">Student id : </label>\n      <div class=\"col-sm-10\">\n        <input type=\"text\" class=\"form-control\" id=\"studentId\" placeholder=\"Student id\">\n      </div>\n    </div>\n    <div class=\"form-group\">\n      <label class=\"col-sm-2 control-label\">Student name : </label>\n      <div class=\"col-sm-10\">\n        <input type=\"text\" class=\"form-control\" id=\"studentName\" placeholder=\"Name\">\n      </div>\n    </div>\n    <div class=\"form-group\">\n      <label class=\"col-sm-2 control-label\">Pen amount : </label>\n      <div class=\"col-sm-10 form-inline\">\n        <button type=\"button\" class=\"btn btn-primary btn-xs col-md-1 button-amount\" (click)=\"downQuantity(student)\">-\n        </button>\n        <input type=\"text\" class=\"col-md-1 form-control input-amount\" value=\"0\">\n        <button type=\"button\" class=\"btn btn-primary btn-xs col-md-1 button-amount\" (click)=\"upQuantity(student)\">+\n        </button>\n      </div>\n    </div>\n    <div class=\"form-group\">\n      <label class=\"col-sm-2 control-label\">File input : </label>\n      <div class=\"col-sm-10\">\n        <input type=\"file\" class=\"btn btn-default\" id=\"exampleInputFile\">\n        <p class=\"help-block\">Leave here for the file preview later.</p>\n      </div>\n    </div>\n    <div class=\"form-group\">\n      <label class=\"col-sm-2 control-label\">Description : </label>\n      <div class=\"col-sm-10\">\n        <textarea class=\"form-control\" id=\"studentDescription\" placeholder=\"Description\" row=\"3\"></textarea>\n      </div>\n    </div>\n    <div class=\"form-group\">\n      <div class=\"col-sm-offset-2 col-sm-10\">\n        <button type=\"submit\" class=\"btn btn-default\">Add</button>\n      </div>\n    </div>\n  </form>\n</div>\n-->\n<mat-toolbar>\n    <span>Add new student</span>\n</mat-toolbar>\n<form style=\"margin-left: 1em;\">\n\n    <mat-form-field class=\"full-width\">\n      <input matInput placeholder=\"Student Id\">\n    </mat-form-field>\n  \n    <table class=\"full-width\">\n      <tr>\n        <td>\n            <mat-form-field class=\"full-width\">\n              <input matInput placeholder=\"First name\">\n            </mat-form-field>\n        </td>\n        <td>\n            <mat-form-field class=\"full-width\">\n              <input matInput placeholder=\"Surname\">\n            </mat-form-field>\n        </td>\n      </tr>\n        <div>\n          <button mat-button class=\"center-button mat-raised-button mat-button-base\" (click)=\"downQuantity(student)\">-</button>\n          \n          <mat-form-field class=\"penInput\">\n            <input matInput class=\"center-text\"  placeholder=\"Pen Amount\" value=\"0\">\n          </mat-form-field>\n          <button mat-button class=\"center-button mat-raised-button mat-button-base\" (click)=\"upQuantity(student)\">+</button>\n        </div>\n    </table>\n\n    <mat-form-field class=\"full-width\">\n        <input matInput placeholder=\"File name\">\n        <mat-hint>Will change to the image uploader component later, hell the page doesn't even work. Don't expect much. I am certainly he gonna let us continue this in his project.</mat-hint>\n      </mat-form-field>\n  \n    \n  \n    <mat-form-field class=\"full-width\">\n        <textarea matInput placeholder=\"Description\" ></textarea>\n    </mat-form-field>\n\n    <div class=\"center\">\n      <button mat-button class=\"mat-raised-button\" color=\"primary\">Add</button>\n    </div>\n  \n  </form>\n\n\n\n \n\n\n\n\n\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/students/list/students.component.html":
/*!*********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/students/list/students.component.html ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngFor=\"let student of students\" class=\"row\">\n    <div class=\"panel panel-primary col-md-offset-2 col-md-6 col-sm-offset-1 col-sm-8\">\n        <div class=\"panel-heading\">\n            <h2 class=\"panel-title\">{{student.studentId }}</h2>\n        </div>\n        <div class=\"panel-body row\">\n            <div class=\"col-md-6 col-sm-6\">\n                <p class=\"student-name\"> {{student. name | uppercase}} {{student.surname}}</p>\n                <p *ngIf=\"student.gpa > 2.5\">Good Student who get grade {{student.gpa | number:'1.2-2'}}</p>\n                <p *ngIf=\"student.gpa <= 2.5\">Bad Student who get grade {{student.gpa | number:'1.2-2'}}</p>\n                <div class=\"row\">\n                    <div class=\"col-sm-offset-1 \">\n                        <button type=\"button\" class=\"btn btn-primary btn-xs col-md-1\" (click)=\"downQuantity(student)\">-</button>\n                        <input type=\"text\" class=\"col-md-1\" />\n                        <button type=\"button\" class=\"btn btn-primary btn-xs col-md-1\" (click)=\"upQuantity(student)\">+</button>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-md-4 col-sm-6\">\n                <img class=\"img-responsive\" [src]=\"student.image\" [title]=\"student.name\">\n            </div>\n        </div>\n    </div>\n</div>\n\n<div class=\"alert alert-success col-md-offset-2 col-md-6\"> The average gpa is {{averageGpa()|number:'1.2-2'}} </div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/students/student-table/student-table.component.html":
/*!***********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/students/student-table/student-table.component.html ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"mat-elevation-z8\">\n  <table mat-table class=\"full-width-table\" matSort aria-label=\"Elements\">\n    <!-- Id Column -->\n    <ng-container matColumnDef=\"id\">\n      <th mat-header-cell *matHeaderCellDef>Id</th>\n      <td mat-cell *matCellDef=\"let student\">{{student.id}}</td>\n      <td mat-footer-cell *matFooterCellDef></td> \n    </ng-container>\n\n    <!-- studentId Column -->\n    <ng-container matColumnDef=\"studentId\">\n      <th mat-header-cell *matHeaderCellDef>Student Id</th>\n      <td mat-cell *matCellDef=\"let student\">{{student.studentId}}</td>\n      <td mat-footer-cell *matFooterCellDef></td> \n    </ng-container>\n\n    <!-- Name Column -->\n    <ng-container matColumnDef=\"name\">\n      <th mat-header-cell *matHeaderCellDef mat-sort-header>Name</th>\n      <td mat-cell *matCellDef=\"let student\">{{student.name}}</td>\n      <td mat-footer-cell *matFooterCellDef>Total</td>\n    </ng-container>\n\n    <!-- surName Column -->\n    <ng-container matColumnDef=\"surname\">\n      <th mat-header-cell *matHeaderCellDef mat-sort-header>Surname</th>\n      <td mat-cell *matCellDef=\"let student\">{{student.surname}}</td>\n      <td mat-footer-cell *matFooterCellDef></td> \n    </ng-container>\n\n    <!-- img Column -->\n    <ng-container matColumnDef=\"image\">\n      <th mat-header-cell *matHeaderCellDef class=\"center-text\">Image</th>\n      <td mat-cell *matCellDef=\"let student\" class=\"center-text\">\n        <img [src]=\"student.image\" [title]=\"student.name\" class=\"img-fluid\">\n      </td>\n      <td mat-footer-cell *matFooterCellDef></td> \n  </ng-container>\n\n  <!-- penamount Column bugnow -->\n  <ng-container matColumnDef=\"penAmount\">\n    <th mat-header-cell *matHeaderCellDef>PenAmount</th>\n    <td mat-cell *matCellDef=\"let student\">\n      \n          <button mat-raised-bunton class=\"center-bunton\" (click)=\"downQuantity(student)\">-</button>\n          <!-- {{student.penAmount}} -->\n          <mat-form-field class=\"penInput\">\n            <input matInput class=\"center-text penInput\" [(ngModel)]=\"student.penAmount\">\n          </mat-form-field> \n  \n          <button mat-raised-bunton class=\"center-bunton\" (click)=\"upQuantity(student)\">+</button>\n        \n    </td>\n    <td mat-footer-cell *matFooterCellDef></td> \n  </ng-container>\n\n    \n    \n\n     <!-- gpa Column -->\n     <ng-container matColumnDef=\"gpa\">\n        <th mat-header-cell *matHeaderCellDef  mat-sort-header >GPA</th>\n        <td mat-cell *matCellDef=\"let student\" >{{student.gpa}}</td>    \n        <td mat-footer-cell *matFooterCellDef >{{averageGpa() | number:'1.2-2'}}</td> \n        <td mat-footer-cell *matFooterCellDef></td> \n    </ng-container>\n\n\n    <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\n    <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\n    <tr mat-footer-row *matFooterRowDef=\"displayedColumns\"></tr>\n  </table>\n\n  <mat-paginator #paginator\n      [length]=\"dataSource?.data.length\"\n      [pageIndex]=\"0\"\n      [pageSize]=\"50\"\n      [pageSizeOptions]=\"[25, 50, 100, 250]\">\n  </mat-paginator>\n</div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/students/view/students.view.component.html":
/*!**************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/students/view/students.view.component.html ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Old\n  <div class=\"col-md-offset-1 col-md-4\">\n  <form class=\"form-horizontal \">\n    <div class=\"form-group\">\n      <label class=\"col-sm-2 control-label\">Student id : </label>\n      <div class=\"col-sm-10\">\n        {{student?.id}}\n      </div>\n    </div>\n    <div class=\"form-group\">\n      <label class=\"col-sm-2 control-label\">Student name : </label>\n      <div class=\"col-sm-10\">\n        {{student.name}} {{student.surname}}\n      </div>\n    </div>\n    <div class=\"form-group\">\n      <label class=\"col-sm-2 control-label\">Pen amount : </label>\n      <div class=\"col-sm-10 form-inline\">\n        {{student.penAmount}}\n      </div>\n    </div>\n    <div class=\"form-group\">\n      <label class=\"col-sm-2 control-label\">File input : </label>\n      <div class=\"col-sm-10\">\n        <img class=\"img-responsive\" [src]=\"student.image\" [alt]=\"student.image\">\n        <p class=\"help-block\">Leave here for the file preview later.</p>\n      </div>\n    </div>\n    <div class=\"form-group\">\n      <label class=\"col-sm-2 control-label\">Description : </label>\n      <div class=\"col-sm-10\">\n        {{student.description}}\n      </div>\n    </div>\n    <div class=\"form-group\">\n      <div class=\"col-sm-offset-2 col-sm-10\">\n        <button type=\"submit\" class=\"btn btn-default\">Add</button>\n      </div>\n    </div>\n  </form>\n</div> -->\n\n<mat-toolbar>\n    <span>Software Engineering</span>\n  </mat-toolbar>\n  <form style=\"margin-left: 1em;\">\n  \n    <mat-form-field class=\"full-width\">\n      <input matInput placeholder=\"Student Id\" [value]=\"student.studentId\" readonly >\n    </mat-form-field>\n  \n    <table  class=\"full-width\">\n      <tr>\n        <td>\n            <mat-form-field class=\"full-width\">\n              <input matInput placeholder=\"First name\" [value]=\"student.name\" readonly >\n            </mat-form-field>\n        </td>\n        <td>\n            <mat-form-field class=\"full-width\">\n              <input matInput placeholder=\"Surname\" [value]=\"student.surname\" readonly >\n            </mat-form-field>\n        </td>\n      </tr>\n          <mat-form-field class=\"penInput\">\n            <input matInput class=\"center-text\"  placeholder=\"Age\" [value]=\"student.penAmount\" readonly >\n          </mat-form-field>\n    </table>\n  \n    <div class=\"center\">\n      <mat-card class=\"img-width\">\n        <mat-card-header>\n          <div class=\"mat-card-header-text\">\n              <mat-card-title ></mat-card-title>\n            <mat-card-title >Image</mat-card-title>\n          </div>\n        </mat-card-header>\n        <img class=\"mat-card-image\" [src]=\"student.image\" [alt]=\"student.image\">\n      </mat-card>\n    </div>\n  \n    <mat-form-field class=\"full-width\">\n        <textarea matInput placeholder=\"Description\" [value]=\"student.description\" readonly ></textarea>\n    </mat-form-field>\n  \n  </form>\n  \n  \n  \n  \n"

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _shared_file_not_found_file_not_found_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./shared/file-not-found/file-not-found.component */ "./src/app/shared/file-not-found/file-not-found.component.ts");




const appRoutes = [
    {
        path: '', redirectTo: '/detail/1', pathMatch: 'full'
    },
    { path: '**', component: _shared_file_not_found_file_not_found_component__WEBPACK_IMPORTED_MODULE_3__["FileNotFoundComponent"] }
];
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(appRoutes)
        ],
        exports: [
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]
        ]
    })
], AppRoutingModule);

//export const AppRoutingModule: ModuleWithProviders = RouterModule.forRoot(appRoutes);


/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let AppComponent = class AppComponent {
    constructor() {
        this.name = 'SE331';
    }
};
AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-root',
        template: __webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/index.js!./src/app/app.component.html"),
        styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
    })
], AppComponent);



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _service_student_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./service/student-service */ "./src/app/service/student-service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _service_students_file_impl_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./service/students-file-impl.service */ "./src/app/service/students-file-impl.service.ts");
/* harmony import */ var _students_list_students_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./students/list/students.component */ "./src/app/students/list/students.component.ts");
/* harmony import */ var _students_add_students_add_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./students/add/students.add.component */ "./src/app/students/add/students.add.component.ts");
/* harmony import */ var _students_view_students_view_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./students/view/students.view.component */ "./src/app/students/view/students.view.component.ts");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm2015/animations.js");
/* harmony import */ var _my_nav_my_nav_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./my-nav/my-nav.component */ "./src/app/my-nav/my-nav.component.ts");
/* harmony import */ var _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/cdk/layout */ "./node_modules/@angular/cdk/esm2015/layout.js");
/* harmony import */ var _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/material/toolbar */ "./node_modules/@angular/material/esm2015/toolbar.js");
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/material/button */ "./node_modules/@angular/material/esm2015/button.js");
/* harmony import */ var _angular_material_sidenav__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/material/sidenav */ "./node_modules/@angular/material/esm2015/sidenav.js");
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @angular/material/icon */ "./node_modules/@angular/material/esm2015/icon.js");
/* harmony import */ var _angular_material_list__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @angular/material/list */ "./node_modules/@angular/material/esm2015/list.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _shared_file_not_found_file_not_found_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./shared/file-not-found/file-not-found.component */ "./src/app/shared/file-not-found/file-not-found.component.ts");
/* harmony import */ var _students_student_routing_module__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./students/student-routing.module */ "./src/app/students/student-routing.module.ts");
/* harmony import */ var _students_student_table_student_table_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./students/student-table/student-table.component */ "./src/app/students/student-table/student-table.component.ts");
/* harmony import */ var _angular_material_table__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! @angular/material/table */ "./node_modules/@angular/material/esm2015/table.js");
/* harmony import */ var _angular_material_paginator__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! @angular/material/paginator */ "./node_modules/@angular/material/esm2015/paginator.js");
/* harmony import */ var _angular_material_sort__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! @angular/material/sort */ "./node_modules/@angular/material/esm2015/sort.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");
/* harmony import */ var _angular_material_card__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! @angular/material/card */ "./node_modules/@angular/material/esm2015/card.js");





























let AppModule = class AppModule {
};
AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        declarations: [
            _app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"],
            _students_list_students_component__WEBPACK_IMPORTED_MODULE_8__["StudentsComponent"],
            _students_add_students_add_component__WEBPACK_IMPORTED_MODULE_9__["StudentsAddComponent"],
            _students_view_students_view_component__WEBPACK_IMPORTED_MODULE_10__["StudentsViewComponent"],
            _my_nav_my_nav_component__WEBPACK_IMPORTED_MODULE_12__["MyNavComponent"],
            _shared_file_not_found_file_not_found_component__WEBPACK_IMPORTED_MODULE_20__["FileNotFoundComponent"],
            _students_student_table_student_table_component__WEBPACK_IMPORTED_MODULE_22__["StudentTableComponent"]
        ],
        imports: [
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClientModule"],
            _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_11__["BrowserAnimationsModule"],
            _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_13__["LayoutModule"],
            _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_14__["MatToolbarModule"],
            _angular_material_button__WEBPACK_IMPORTED_MODULE_15__["MatButtonModule"],
            _angular_material_sidenav__WEBPACK_IMPORTED_MODULE_16__["MatSidenavModule"],
            _angular_material_icon__WEBPACK_IMPORTED_MODULE_17__["MatIconModule"],
            _angular_material_list__WEBPACK_IMPORTED_MODULE_18__["MatListModule"],
            _students_student_routing_module__WEBPACK_IMPORTED_MODULE_21__["StudentRoutingModule"],
            _app_routing_module__WEBPACK_IMPORTED_MODULE_19__["AppRoutingModule"],
            _angular_material_table__WEBPACK_IMPORTED_MODULE_23__["MatTableModule"],
            _angular_material_paginator__WEBPACK_IMPORTED_MODULE_24__["MatPaginatorModule"],
            _angular_material_sort__WEBPACK_IMPORTED_MODULE_25__["MatSortModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_26__["MatFormFieldModule"],
            _angular_material_card__WEBPACK_IMPORTED_MODULE_27__["MatCardModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_26__["MatInputModule"]
        ],
        providers: [
            { provide: _service_student_service__WEBPACK_IMPORTED_MODULE_4__["StudentService"], useClass: _service_students_file_impl_service__WEBPACK_IMPORTED_MODULE_7__["StudentsFileImplService"] }
        ],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]]
    })
], AppModule);



/***/ }),

/***/ "./src/app/my-nav/my-nav.component.css":
/*!*********************************************!*\
  !*** ./src/app/my-nav/my-nav.component.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".sidenav-container {\n  height: 100%;\n}\n\n.sidenav {\n  width: 200px;\n}\n\n.sidenav .mat-toolbar {\n  background: inherit;\n}\n\n.mat-toolbar.mat-primary {\n  position: -webkit-sticky;\n  position: sticky;\n  top: 0;\n  z-index: 1;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbXktbmF2L215LW5hdi5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsWUFBWTtBQUNkOztBQUVBO0VBQ0UsWUFBWTtBQUNkOztBQUVBO0VBQ0UsbUJBQW1CO0FBQ3JCOztBQUVBO0VBQ0Usd0JBQWdCO0VBQWhCLGdCQUFnQjtFQUNoQixNQUFNO0VBQ04sVUFBVTtBQUNaIiwiZmlsZSI6InNyYy9hcHAvbXktbmF2L215LW5hdi5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnNpZGVuYXYtY29udGFpbmVyIHtcbiAgaGVpZ2h0OiAxMDAlO1xufVxuXG4uc2lkZW5hdiB7XG4gIHdpZHRoOiAyMDBweDtcbn1cblxuLnNpZGVuYXYgLm1hdC10b29sYmFyIHtcbiAgYmFja2dyb3VuZDogaW5oZXJpdDtcbn1cblxuLm1hdC10b29sYmFyLm1hdC1wcmltYXJ5IHtcbiAgcG9zaXRpb246IHN0aWNreTtcbiAgdG9wOiAwO1xuICB6LWluZGV4OiAxO1xufVxuIl19 */"

/***/ }),

/***/ "./src/app/my-nav/my-nav.component.ts":
/*!********************************************!*\
  !*** ./src/app/my-nav/my-nav.component.ts ***!
  \********************************************/
/*! exports provided: MyNavComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MyNavComponent", function() { return MyNavComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/cdk/layout */ "./node_modules/@angular/cdk/esm2015/layout.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");




let MyNavComponent = class MyNavComponent {
    constructor(breakpointObserver) {
        this.breakpointObserver = breakpointObserver;
        this.isHandset$ = this.breakpointObserver.observe(_angular_cdk_layout__WEBPACK_IMPORTED_MODULE_2__["Breakpoints"].Handset)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(result => result.matches), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["shareReplay"])());
    }
};
MyNavComponent.ctorParameters = () => [
    { type: _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_2__["BreakpointObserver"] }
];
MyNavComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-my-nav',
        template: __webpack_require__(/*! raw-loader!./my-nav.component.html */ "./node_modules/raw-loader/index.js!./src/app/my-nav/my-nav.component.html"),
        styles: [__webpack_require__(/*! ./my-nav.component.css */ "./src/app/my-nav/my-nav.component.css")]
    })
], MyNavComponent);



/***/ }),

/***/ "./src/app/service/student-service.ts":
/*!********************************************!*\
  !*** ./src/app/service/student-service.ts ***!
  \********************************************/
/*! exports provided: StudentService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StudentService", function() { return StudentService; });
class StudentService {
}


/***/ }),

/***/ "./src/app/service/students-file-impl.service.ts":
/*!*******************************************************!*\
  !*** ./src/app/service/students-file-impl.service.ts ***!
  \*******************************************************/
/*! exports provided: StudentsFileImplService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StudentsFileImplService", function() { return StudentsFileImplService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _student_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./student-service */ "./src/app/service/student-service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");





let StudentsFileImplService = class StudentsFileImplService extends _student_service__WEBPACK_IMPORTED_MODULE_3__["StudentService"] {
    constructor(http) {
        super();
        this.http = http;
    }
    getStudents() {
        return this.http.get('assets/people.json');
    }
    getStudent(id) {
        return this.http.get('assets/people.json').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(Students => {
            const output = Students.find(student => student.id === +id);
            return output;
        }));
    }
};
StudentsFileImplService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
StudentsFileImplService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], StudentsFileImplService);



/***/ }),

/***/ "./src/app/shared/file-not-found/file-not-found.component.css":
/*!********************************************************************!*\
  !*** ./src/app/shared/file-not-found/file-not-found.component.css ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "img{\n    width: 200px;\n    height: auto;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkL2ZpbGUtbm90LWZvdW5kL2ZpbGUtbm90LWZvdW5kLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxZQUFZO0lBQ1osWUFBWTtBQUNoQiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9maWxlLW5vdC1mb3VuZC9maWxlLW5vdC1mb3VuZC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW1ne1xuICAgIHdpZHRoOiAyMDBweDtcbiAgICBoZWlnaHQ6IGF1dG87XG59Il19 */"

/***/ }),

/***/ "./src/app/shared/file-not-found/file-not-found.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/shared/file-not-found/file-not-found.component.ts ***!
  \*******************************************************************/
/*! exports provided: FileNotFoundComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FileNotFoundComponent", function() { return FileNotFoundComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let FileNotFoundComponent = class FileNotFoundComponent {
    constructor() { }
    ngOnInit() {
    }
};
FileNotFoundComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-file-not-found',
        template: __webpack_require__(/*! raw-loader!./file-not-found.component.html */ "./node_modules/raw-loader/index.js!./src/app/shared/file-not-found/file-not-found.component.html"),
        styles: [__webpack_require__(/*! ./file-not-found.component.css */ "./src/app/shared/file-not-found/file-not-found.component.css")]
    })
], FileNotFoundComponent);



/***/ }),

/***/ "./src/app/students/add/students.add.component.css":
/*!*********************************************************!*\
  !*** ./src/app/students/add/students.add.component.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n\n.button-amount{\n\tbox-shadow: 0 8px 16px 0 rgba(0,0,0,0.2), 0 6px 20px 0 rgba(0,0,0,0.19);\n\tmargin-top: 2px;\n}\n\n.input-amount{\n\tbox-sizing: border-box;\n    border: 2px solid #ccc;\n    border-radius: 4px;\n\tmargin-left: 2px;\n\tmargin-right: 2px;\n\twidth: 60px;\n\ttext-align: center;\n\n}\n\n.full-width {\n\twidth:100%;\n}\n\n.center-button {\n\ttext-align: center;\n\tpadding-left: .5em;\n\tmin-width: 0.5rem;\n\tmax-width: 0.5rem;\n  }\n\n.center-text {\n\ttext-align: center;\n  }\n\n.center {\n\tdisplay: flex;\n\tjustify-content: center;\n  }\n\n.penInput {\n\t  min-width:3rem;\n\t  max-width: 6rem;\n\t}\n\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc3R1ZGVudHMvYWRkL3N0dWRlbnRzLmFkZC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBRUE7Q0FDQyx1RUFBdUU7Q0FDdkUsZUFBZTtBQUNoQjs7QUFFQTtDQUNDLHNCQUFzQjtJQUNuQixzQkFBc0I7SUFDdEIsa0JBQWtCO0NBQ3JCLGdCQUFnQjtDQUNoQixpQkFBaUI7Q0FDakIsV0FBVztDQUNYLGtCQUFrQjs7QUFFbkI7O0FBRUE7Q0FDQyxVQUFVO0FBQ1g7O0FBQ0E7Q0FDQyxrQkFBa0I7Q0FDbEIsa0JBQWtCO0NBQ2xCLGlCQUFpQjtDQUNqQixpQkFBaUI7RUFDaEI7O0FBQ0E7Q0FDRCxrQkFBa0I7RUFDakI7O0FBRUE7Q0FDRCxhQUFhO0NBQ2IsdUJBQXVCO0VBQ3RCOztBQUNBO0dBQ0MsY0FBYztHQUNkLGVBQWU7Q0FDakIiLCJmaWxlIjoic3JjL2FwcC9zdHVkZW50cy9hZGQvc3R1ZGVudHMuYWRkLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcblxuLmJ1dHRvbi1hbW91bnR7XG5cdGJveC1zaGFkb3c6IDAgOHB4IDE2cHggMCByZ2JhKDAsMCwwLDAuMiksIDAgNnB4IDIwcHggMCByZ2JhKDAsMCwwLDAuMTkpO1xuXHRtYXJnaW4tdG9wOiAycHg7XG59XG5cbi5pbnB1dC1hbW91bnR7XG5cdGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG4gICAgYm9yZGVyOiAycHggc29saWQgI2NjYztcbiAgICBib3JkZXItcmFkaXVzOiA0cHg7XG5cdG1hcmdpbi1sZWZ0OiAycHg7XG5cdG1hcmdpbi1yaWdodDogMnB4O1xuXHR3aWR0aDogNjBweDtcblx0dGV4dC1hbGlnbjogY2VudGVyO1xuXG59XG5cbi5mdWxsLXdpZHRoIHtcblx0d2lkdGg6MTAwJTtcbn1cbi5jZW50ZXItYnV0dG9uIHtcblx0dGV4dC1hbGlnbjogY2VudGVyO1xuXHRwYWRkaW5nLWxlZnQ6IC41ZW07XG5cdG1pbi13aWR0aDogMC41cmVtO1xuXHRtYXgtd2lkdGg6IDAuNXJlbTtcbiAgfVxuICAuY2VudGVyLXRleHQge1xuXHR0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIH1cbiAgXG4gIC5jZW50ZXIge1xuXHRkaXNwbGF5OiBmbGV4O1xuXHRqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgfVxuICAucGVuSW5wdXQge1xuXHQgIG1pbi13aWR0aDozcmVtO1xuXHQgIG1heC13aWR0aDogNnJlbTtcblx0fVxuXG4iXX0= */"

/***/ }),

/***/ "./src/app/students/add/students.add.component.ts":
/*!********************************************************!*\
  !*** ./src/app/students/add/students.add.component.ts ***!
  \********************************************************/
/*! exports provided: StudentsAddComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StudentsAddComponent", function() { return StudentsAddComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_service_student_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/service/student-service */ "./src/app/service/student-service.ts");



let StudentsAddComponent = class StudentsAddComponent {
    constructor(studentService) {
        this.studentService = studentService;
    }
    upQuantity(student) {
        student.penAmount++;
    }
    downQuantity(student) {
        if (student.penAmount > 0) {
            student.penAmount--;
        }
    }
};
StudentsAddComponent.ctorParameters = () => [
    { type: src_app_service_student_service__WEBPACK_IMPORTED_MODULE_2__["StudentService"] }
];
StudentsAddComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-students-add',
        template: __webpack_require__(/*! raw-loader!./students.add.component.html */ "./node_modules/raw-loader/index.js!./src/app/students/add/students.add.component.html"),
        styles: [__webpack_require__(/*! ./students.add.component.css */ "./src/app/students/add/students.add.component.css")]
    })
], StudentsAddComponent);



/***/ }),

/***/ "./src/app/students/list/students.component.css":
/*!******************************************************!*\
  !*** ./src/app/students/list/students.component.css ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".student-name {\n    color: #FF0000;\n    font-size: x-large;\n}\n\n.grade {\n    font-weight: bold;\n}\n\n.featured {\n    background: linear-gradient(to bottom right, red, yellow);\n}\n\nbutton {\n    box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);\n    margin-top: 2px;\n}\n\ninput {\n    box-sizing: border-box;\n    border: 2px solid #ccc;\n    border-radius: 4px;\n    margin-left: 2px;\n    margin-right: 2px;\n    width: 60px;\n    text-align: center;\n}\n\nimg {\n    height: auto;\n    width: 200px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc3R1ZGVudHMvbGlzdC9zdHVkZW50cy5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksY0FBYztJQUNkLGtCQUFrQjtBQUN0Qjs7QUFFQTtJQUNJLGlCQUFpQjtBQUNyQjs7QUFFQTtJQUNJLHlEQUF5RDtBQUM3RDs7QUFFQTtJQUNJLDZFQUE2RTtJQUM3RSxlQUFlO0FBQ25COztBQUVBO0lBQ0ksc0JBQXNCO0lBQ3RCLHNCQUFzQjtJQUN0QixrQkFBa0I7SUFDbEIsZ0JBQWdCO0lBQ2hCLGlCQUFpQjtJQUNqQixXQUFXO0lBQ1gsa0JBQWtCO0FBQ3RCOztBQUVBO0lBQ0ksWUFBWTtJQUNaLFlBQVk7QUFDaEIiLCJmaWxlIjoic3JjL2FwcC9zdHVkZW50cy9saXN0L3N0dWRlbnRzLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuc3R1ZGVudC1uYW1lIHtcbiAgICBjb2xvcjogI0ZGMDAwMDtcbiAgICBmb250LXNpemU6IHgtbGFyZ2U7XG59XG5cbi5ncmFkZSB7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG5cbi5mZWF0dXJlZCB7XG4gICAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KHRvIGJvdHRvbSByaWdodCwgcmVkLCB5ZWxsb3cpO1xufVxuXG5idXR0b24ge1xuICAgIGJveC1zaGFkb3c6IDAgOHB4IDE2cHggMCByZ2JhKDAsIDAsIDAsIDAuMiksIDAgNnB4IDIwcHggMCByZ2JhKDAsIDAsIDAsIDAuMTkpO1xuICAgIG1hcmdpbi10b3A6IDJweDtcbn1cblxuaW5wdXQge1xuICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG4gICAgYm9yZGVyOiAycHggc29saWQgI2NjYztcbiAgICBib3JkZXItcmFkaXVzOiA0cHg7XG4gICAgbWFyZ2luLWxlZnQ6IDJweDtcbiAgICBtYXJnaW4tcmlnaHQ6IDJweDtcbiAgICB3aWR0aDogNjBweDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbmltZyB7XG4gICAgaGVpZ2h0OiBhdXRvO1xuICAgIHdpZHRoOiAyMDBweDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/students/list/students.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/students/list/students.component.ts ***!
  \*****************************************************/
/*! exports provided: StudentsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StudentsComponent", function() { return StudentsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _service_student_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../service/student-service */ "./src/app/service/student-service.ts");



let StudentsComponent = class StudentsComponent {
    constructor(studentService) {
        this.studentService = studentService;
    }
    ngOnInit() {
        this.studentService.getStudents()
            .subscribe(students => this.students = students);
    }
    averageGpa() {
        let sum = 0;
        if (Array.isArray(this.students)) {
            for (const student of this.students) {
                sum += student.gpa;
            }
            return sum / this.students.length;
        }
        else {
            return null;
        }
    }
    upQuantity(student) {
        student.penAmount++;
    }
    downQuantity(student) {
        if (student.penAmount > 0) {
            student.penAmount--;
        }
    }
};
StudentsComponent.ctorParameters = () => [
    { type: _service_student_service__WEBPACK_IMPORTED_MODULE_2__["StudentService"] }
];
StudentsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-students',
        template: __webpack_require__(/*! raw-loader!./students.component.html */ "./node_modules/raw-loader/index.js!./src/app/students/list/students.component.html"),
        styles: [__webpack_require__(/*! ./students.component.css */ "./src/app/students/list/students.component.css")]
    })
], StudentsComponent);



/***/ }),

/***/ "./src/app/students/student-routing.module.ts":
/*!****************************************************!*\
  !*** ./src/app/students/student-routing.module.ts ***!
  \****************************************************/
/*! exports provided: StudentRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StudentRoutingModule", function() { return StudentRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _view_students_view_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./view/students.view.component */ "./src/app/students/view/students.view.component.ts");
/* harmony import */ var _add_students_add_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./add/students.add.component */ "./src/app/students/add/students.add.component.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _student_table_student_table_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./student-table/student-table.component */ "./src/app/students/student-table/student-table.component.ts");






const StudentRoutes = [
    //{path: 'view', component: StudentsViewComponent},
    { path: 'add', component: _add_students_add_component__WEBPACK_IMPORTED_MODULE_3__["StudentsAddComponent"] },
    { path: 'list', component: _student_table_student_table_component__WEBPACK_IMPORTED_MODULE_5__["StudentTableComponent"] },
    { path: 'detail/:id', component: _view_students_view_component__WEBPACK_IMPORTED_MODULE_2__["StudentsViewComponent"] }
];
let StudentRoutingModule = class StudentRoutingModule {
};
StudentRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["NgModule"])({
        imports: [
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(StudentRoutes)
        ],
        exports: [
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]
        ]
    })
], StudentRoutingModule);



/***/ }),

/***/ "./src/app/students/student-table/student-table-datasource.ts":
/*!********************************************************************!*\
  !*** ./src/app/students/student-table/student-table-datasource.ts ***!
  \********************************************************************/
/*! exports provided: StudentTableDataSource */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StudentTableDataSource", function() { return StudentTableDataSource; });
/* harmony import */ var _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/cdk/collections */ "./node_modules/@angular/cdk/esm2015/collections.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");



// TODO: replace this with real data from your application
const EXAMPLE_DATA = [];
/**
 * Data source for the StudentTable view. This class should
 * encapsulate all logic for fetching and manipulating the displayed data
 * (including sorting, pagination, and filtering).
 */
class StudentTableDataSource extends _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_0__["DataSource"] {
    constructor() {
        super();
        this.data = EXAMPLE_DATA;
    }
    /**
     * Connect this data source to the table. The table will only update when
     * the returned stream emits new items.
     * @returns A stream of the items to be rendered.
     */
    connect() {
        // Combine everything that affects the rendered data into one update
        // stream for the data-table to consume.
        const dataMutations = [
            Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["of"])(this.data),
            this.paginator.page,
            this.sort.sortChange,
            this.filter$.asObservable()
        ];
        this.paginator.length = this.data.length;
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["merge"])(...dataMutations).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["map"])(() => {
            return this.getPagedData(this.getSortedData([...this.data]));
        }));
    }
    /**
     *  Called when the table is being destroyed. Use this function, to clean up
     * any open connections or free any held resources that were set up during connect.
     */
    disconnect() { }
    /**
     * Paginate the data (client-side). If you're using server-side pagination,
     * this would be replaced by requesting the appropriate data from the server.
     */
    getPagedData(data) {
        const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
        return data.splice(startIndex, this.paginator.pageSize);
    }
    /**
     * Sort the data (client-side). If you're using server-side sorting,
     * this would be replaced by requesting the appropriate data from the server.
     */
    getSortedData(data) {
        if (!this.sort.active || this.sort.direction === '') {
            return data;
        }
        return data.sort((a, b) => {
            const isAsc = this.sort.direction === 'asc';
            switch (this.sort.active) {
                case 'name': return compare(a.name, b.name, isAsc);
                case 'id': return compare(+a.id, +b.id, isAsc);
                case 'surname': return compare(+a.surname, +b.surname, isAsc);
                default: return 0;
            }
        });
    }
    getFilter(data) {
        const filter = this.filter$.getValue();
        if (filter === '') {
            return data;
        }
        return data.filter((student) => {
            return (student.name.toLowerCase().includes(filter) || student.surname.toLowerCase().includes(filter));
        });
    }
}
/** Simple sort comparator for example ID/Name columns (for client-side sorting). */
function compare(a, b, isAsc) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}


/***/ }),

/***/ "./src/app/students/student-table/student-table.component.css":
/*!********************************************************************!*\
  !*** ./src/app/students/student-table/student-table.component.css ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".full-width-table {\n  width: 100%;\n}\ntr.mat-footer-row {\n  font-weight: bold;\n}\nimg {\n  /*height: auto;\n  width: 200px;*/\n  max-width: 20rem;\n  max-height: 20rem;\n}\n.center-text {\n\ttext-align: center;\n  }\n.center-button {\n    text-align: center;\n    padding-left: .5em;\n  }\n.center {\n\tdisplay: flex;\n\tjustify-content: center;\n  }\n.mat-raised-button {\n    min-width: 0.5rem;\n    max-width: 0.5rem;\n}\n.mat-form-field {\n  font-size: 14px;\n  width: 100%;\n}\n.penInput {\n  min-width: 2rem;\n  max-width: 2rem;\n}\n.mat-cell {\n  padding: 8px 8px 8px 0;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc3R1ZGVudHMvc3R1ZGVudC10YWJsZS9zdHVkZW50LXRhYmxlLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxXQUFXO0FBQ2I7QUFDQTtFQUNFLGlCQUFpQjtBQUNuQjtBQUVBO0VBQ0U7Z0JBQ2M7RUFDZCxnQkFBZ0I7RUFDaEIsaUJBQWlCO0FBQ25CO0FBQ0E7Q0FDQyxrQkFBa0I7RUFDakI7QUFDQTtJQUNFLGtCQUFrQjtJQUNsQixrQkFBa0I7RUFDcEI7QUFFQTtDQUNELGFBQWE7Q0FDYix1QkFBdUI7RUFDdEI7QUFDQTtJQUNFLGlCQUFpQjtJQUNqQixpQkFBaUI7QUFDckI7QUFDQTtFQUNFLGVBQWU7RUFDZixXQUFXO0FBQ2I7QUFDQTtFQUNFLGVBQWU7RUFDZixlQUFlO0FBQ2pCO0FBQ0E7RUFDRSxzQkFBc0I7QUFDeEIiLCJmaWxlIjoic3JjL2FwcC9zdHVkZW50cy9zdHVkZW50LXRhYmxlL3N0dWRlbnQtdGFibGUuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5mdWxsLXdpZHRoLXRhYmxlIHtcbiAgd2lkdGg6IDEwMCU7XG59XG50ci5tYXQtZm9vdGVyLXJvdyB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuXG5pbWcge1xuICAvKmhlaWdodDogYXV0bztcbiAgd2lkdGg6IDIwMHB4OyovXG4gIG1heC13aWR0aDogMjByZW07XG4gIG1heC1oZWlnaHQ6IDIwcmVtO1xufVxuLmNlbnRlci10ZXh0IHtcblx0dGV4dC1hbGlnbjogY2VudGVyO1xuICB9XG4gIC5jZW50ZXItYnV0dG9uIHtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgcGFkZGluZy1sZWZ0OiAuNWVtO1xuICB9XG4gIFxuICAuY2VudGVyIHtcblx0ZGlzcGxheTogZmxleDtcblx0anVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIH1cbiAgLm1hdC1yYWlzZWQtYnV0dG9uIHtcbiAgICBtaW4td2lkdGg6IDAuNXJlbTtcbiAgICBtYXgtd2lkdGg6IDAuNXJlbTtcbn1cbi5tYXQtZm9ybS1maWVsZCB7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgd2lkdGg6IDEwMCU7XG59XG4ucGVuSW5wdXQge1xuICBtaW4td2lkdGg6IDJyZW07XG4gIG1heC13aWR0aDogMnJlbTtcbn1cbi5tYXQtY2VsbCB7XG4gIHBhZGRpbmc6IDhweCA4cHggOHB4IDA7XG59XG4iXX0= */"

/***/ }),

/***/ "./src/app/students/student-table/student-table.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/students/student-table/student-table.component.ts ***!
  \*******************************************************************/
/*! exports provided: StudentTableComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StudentTableComponent", function() { return StudentTableComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_material_paginator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/paginator */ "./node_modules/@angular/material/esm2015/paginator.js");
/* harmony import */ var _angular_material_sort__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material/sort */ "./node_modules/@angular/material/esm2015/sort.js");
/* harmony import */ var _angular_material_table__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/table */ "./node_modules/@angular/material/esm2015/table.js");
/* harmony import */ var _student_table_datasource__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./student-table-datasource */ "./src/app/students/student-table/student-table-datasource.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _service_student_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../service/student-service */ "./src/app/service/student-service.ts");








let StudentTableComponent = class StudentTableComponent {
    constructor(studentService) {
        this.studentService = studentService;
        /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
        this.displayedColumns = ['id', 'studentId', 'name', 'surname', 'image', 'penAmount', 'gpa'];
    }
    ngOnInit() {
        this.studentService.getStudents()
            .subscribe(students => {
            this.dataSource = new _student_table_datasource__WEBPACK_IMPORTED_MODULE_5__["StudentTableDataSource"]();
            this.dataSource.data = students;
            this.dataSource.sort = this.sort;
            this.dataSource.paginator = this.paginator;
            this.filter$ = new rxjs__WEBPACK_IMPORTED_MODULE_6__["BehaviorSubject"]('');
            this.dataSource.filter$ = this.filter$;
            this.table.dataSource = this.dataSource;
            this.students = students;
        });
    }
    ngAfterViewInit() {
    }
    applyFilter(filterValue) {
        this.filter$.next(filterValue.trim().toLowerCase());
    }
    averageGpa() {
        let sum = 0;
        if (Array.isArray(this.students)) {
            for (const student of this.students) {
                sum += student.gpa;
            }
            return sum / this.students.length;
        }
        else {
            return null;
        }
    }
    upQuantity(student) {
        student.penAmount++;
    }
    downQuantity(student) {
        if (student.penAmount > 0) {
            student.penAmount--;
        }
    }
};
StudentTableComponent.ctorParameters = () => [
    { type: _service_student_service__WEBPACK_IMPORTED_MODULE_7__["StudentService"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material_paginator__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"], { static: false })
], StudentTableComponent.prototype, "paginator", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material_sort__WEBPACK_IMPORTED_MODULE_3__["MatSort"], { static: false })
], StudentTableComponent.prototype, "sort", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material_table__WEBPACK_IMPORTED_MODULE_4__["MatTable"], { static: false })
], StudentTableComponent.prototype, "table", void 0);
StudentTableComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-student-table',
        template: __webpack_require__(/*! raw-loader!./student-table.component.html */ "./node_modules/raw-loader/index.js!./src/app/students/student-table/student-table.component.html"),
        styles: [__webpack_require__(/*! ./student-table.component.css */ "./src/app/students/student-table/student-table.component.css")]
    })
], StudentTableComponent);



/***/ }),

/***/ "./src/app/students/view/students.view.component.css":
/*!***********************************************************!*\
  !*** ./src/app/students/view/students.view.component.css ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".button-amount{\n\tbox-shadow: 0 8px 16px 0 rgba(0,0,0,0.2), 0 6px 20px 0 rgba(0,0,0,0.19);\n\tmargin-top: 2px;\n}\n\n.input-amount{\n\tbox-sizing: border-box;\n    border: 2px solid #ccc;\n    border-radius: 4px;\n\tmargin-left: 2px;\n\tmargin-right: 2px;\n\twidth: 60px;\n\ttext-align: center;\n\n}\n\n.full-width {\n\twidth: 100%;\n  }\n\n.penInput {\n\tmin-width: 3rem;\n    max-width: 6rem;\n}\n\n.center-text{\n\ttext-align: center;\n}\n\n.center {\n\tdisplay:flex;\n\tjustify-content: center;\n}\n\n.img-width{\n\tmax-width: 400px;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc3R1ZGVudHMvdmlldy9zdHVkZW50cy52aWV3LmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7Q0FDQyx1RUFBdUU7Q0FDdkUsZUFBZTtBQUNoQjs7QUFFQTtDQUNDLHNCQUFzQjtJQUNuQixzQkFBc0I7SUFDdEIsa0JBQWtCO0NBQ3JCLGdCQUFnQjtDQUNoQixpQkFBaUI7Q0FDakIsV0FBVztDQUNYLGtCQUFrQjs7QUFFbkI7O0FBQ0E7Q0FDQyxXQUFXO0VBQ1Y7O0FBQ0Y7Q0FDQyxlQUFlO0lBQ1osZUFBZTtBQUNuQjs7QUFDQTtDQUNDLGtCQUFrQjtBQUNuQjs7QUFDQTtDQUNDLFlBQVk7Q0FDWix1QkFBdUI7QUFDeEI7O0FBQ0E7Q0FDQyxnQkFBZ0I7QUFDakIiLCJmaWxlIjoic3JjL2FwcC9zdHVkZW50cy92aWV3L3N0dWRlbnRzLnZpZXcuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5idXR0b24tYW1vdW50e1xuXHRib3gtc2hhZG93OiAwIDhweCAxNnB4IDAgcmdiYSgwLDAsMCwwLjIpLCAwIDZweCAyMHB4IDAgcmdiYSgwLDAsMCwwLjE5KTtcblx0bWFyZ2luLXRvcDogMnB4O1xufVxuXG4uaW5wdXQtYW1vdW50e1xuXHRib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICAgIGJvcmRlcjogMnB4IHNvbGlkICNjY2M7XG4gICAgYm9yZGVyLXJhZGl1czogNHB4O1xuXHRtYXJnaW4tbGVmdDogMnB4O1xuXHRtYXJnaW4tcmlnaHQ6IDJweDtcblx0d2lkdGg6IDYwcHg7XG5cdHRleHQtYWxpZ246IGNlbnRlcjtcblxufVxuLmZ1bGwtd2lkdGgge1xuXHR3aWR0aDogMTAwJTtcbiAgfVxuLnBlbklucHV0IHtcblx0bWluLXdpZHRoOiAzcmVtO1xuICAgIG1heC13aWR0aDogNnJlbTtcbn1cbi5jZW50ZXItdGV4dHtcblx0dGV4dC1hbGlnbjogY2VudGVyO1xufVxuLmNlbnRlciB7XG5cdGRpc3BsYXk6ZmxleDtcblx0anVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG59XG4uaW1nLXdpZHRoe1xuXHRtYXgtd2lkdGg6IDQwMHB4O1xufVxuIl19 */"

/***/ }),

/***/ "./src/app/students/view/students.view.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/students/view/students.view.component.ts ***!
  \**********************************************************/
/*! exports provided: StudentsViewComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StudentsViewComponent", function() { return StudentsViewComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _service_student_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../service/student-service */ "./src/app/service/student-service.ts");




let StudentsViewComponent = class StudentsViewComponent {
    constructor(route, studentService) {
        this.route = route;
        this.studentService = studentService;
    }
    ngOnInit() {
        this.student = {
            'id': 2,
            'studentId': '',
            'name': '',
            'surname': '',
            'gpa': 0,
            'image': '',
            'featured': false,
            'penAmount': 0
        };
        this.route.params
            .subscribe((params) => {
            this.studentService.getStudent(params['id'])
                .subscribe((inputStudent) => this.student = inputStudent);
        });
    }
};
StudentsViewComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _service_student_service__WEBPACK_IMPORTED_MODULE_3__["StudentService"] }
];
StudentsViewComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-students-view',
        template: __webpack_require__(/*! raw-loader!./students.view.component.html */ "./node_modules/raw-loader/index.js!./src/app/students/view/students.view.component.html"),
        styles: [__webpack_require__(/*! ./students.view.component.css */ "./src/app/students/view/students.view.component.css")]
    })
], StudentsViewComponent);



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
    production: false
};
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! hammerjs */ "./node_modules/hammerjs/hammer.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm2015/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");





if (_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_3__["AppModule"])
    .catch(err => console.error(err));


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/nebdara/Desktop/Lab/lab05-cv/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main-es2015.js.map